/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  Button,
  AsyncStorage,
  ScrollView
} from 'react-native'
import Moment from 'moment'
import Icon from 'react-native-vector-icons/FontAwesome'
import Hr from 'react-native-hr';
const myIcon = (<Icon name="rocket" size={30} color="#900" />)
export default class App extends Component {
  state = {
    dateLove: '',
    daysLove: 0,
    monthsLove: 0,
    dayInWeek: '',
    weekday: [
      'thứ hai',
      'thứ ba',
      'thứ tư',
      'thứ năm',
      'thứ sáu',
      'thứ bảy',
      'chủ nhật'
    ]
  }

  async componentWillMount() {
    this.GetDay()
  }
  async AddDay() {
    try {
      await AsyncStorage.setItem('@LoveDate:key', '2017-02-06');
    } catch (error) {
      // Error saving data
    }
  }
  async GetDay() {
    try {

      const value = await AsyncStorage.getItem('@LoveDate:key');
      if (value !== null) {
        Moment.locale('vi');
        let dateLove = Moment(value)
        let dateNow = Moment()
        let dayInWeek = this.state.weekday[dateLove.day() - 1]
        let daysLove = dateNow.diff(dateLove, 'days')
        let monthsLove = dateNow.diff(dateLove, 'months')
        this.setState({ dateLove: dateLove.format('DD-MM-YYYY'), dateNow, daysLove, monthsLove, dayInWeek })
      }
      else {
        await this.AddDay();
        await this.GetDay();
      }
    } catch (error) {
      // Error retrieving data
      console.error(error)
    }
  }
  renderMain(){
    var rows = [];
    for (var index = 0; index < 10; index++) {
     rows.push( <View style={styles.main} key={index} >
            <Text style={styles.instructions} >
              Câu chuyện  của chúng mình bắt đầu từ {this.state.dateLove} vào  {this.state.dayInWeek} em à, đi qua {this.state.monthsLove} tháng rồi đó.{'\n'}
              Thế là {this.state.daysLove} ngày, chà chà, lâu rồi chứ.{'\n'} Rồi chúng mình sẽ đi đến đám cưới hehe.{'\n'}
            </Text>
            <Hr lineColor='#fff' text={this.state.monthsLove +' tháng'} textColor='#fff' />
            <Text style={styles.instructions} >
              Hãy quý trọng những gì bạn đang có.{'\n'}
              Đó là tình yêu của bạn.{'\n'}
            </Text>
            
          </View>
     )
    }
    return rows
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#e74c3c"
          barStyle="light-content"
        />
        <ScrollView >
          <View style={styles.titleScrollviewMain} ></View>
         
          {this.renderMain()}
          
        
        </ScrollView>
        <View style={[styles.title]} >
          <View >
            <Image source={require('./img/tuan.jpg')} style={styles.imageAvatar} />
          </View>
          <View style={{ margin: 20, justifyContent: 'center', alignItems: 'center' }} >
            <Icon name="heart" size={30} color="#fff" />
            <Text style={styles.instructions}>
              Love for {this.state.daysLove} day
        </Text>
          </View>
          <View >
            <Image source={require('./img/khanhha.jpg')} style={styles.imageAvatar} />
          </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  title: {
    height: 100,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e74c3c',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 10,
    margin: 10,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  main: {
    flex: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e74c3c',
    margin: 10,
    marginTop: 10,
    marginBottom: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 10,
  },

  titleScrollviewMain: {
    height: 100,
    marginBottom: 10,
  },
  imageAvatar: {
    width: 80,
    height: 80,
    borderRadius: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: '#ecf0f1',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#ecf0f1',
    margin: 10,
  },
});
